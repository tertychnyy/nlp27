FROM caffe2ai/caffe2:c2v0.8.1.cpu.min.ubuntu14.04

MAINTAINER Ivan Tertychnyy "it@chatfirst.co"

ENV XGB_HOME /home
ENV PYTHONPATH=$XGB_HOME/xgboost/python-package

RUN apt update && \

    # Installing debian packages
    apt install -y git make g++ libssl-dev libffi-dev && \

    # xgboost setup
    cd $XGB_HOME && \
    git clone --recursive https://github.com/dmlc/xgboost && \
    cd $XGB_HOME/xgboost && \
    make -j4 && \
    cd $XGB_HOME/xgboost/python-package && \
    pip install scipy scikit-learn && \
    python setup.py install && \

    # Cleaning deb packages
    apt install -y libgomp1 libtk8.6

RUN apt-get -y install enchant

